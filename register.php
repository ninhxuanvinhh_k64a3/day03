<?php
$sex=array('0' => 'Nam', '1' => 'Nữ');
$khoa=array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
?>
<meta charset="UTF-8">
<link rel="stylesheet" href="style.css">

<form action="#">

    <div class="container">
        <div class="content">
            <div class="item">
                <label for="name"> Họ và tên </label>
                <input id="name" type="text">
            </div>

            <div class="sex-title">
                <label for="sex"> Giới tính </label>
                <?php 
                    for ($i = 0; $i <  count($sex); $i++) {
                        echo "<input type=\"radio\" name=\"sex\" value=\"$i\" /> $sex[$i]";
                    };
                ?>
            </div>

            <div class="phankhoa">
                <label>Phân khoa</label>

                <select name="khoa" id="khoa">
                    <option value=" "> </option>
                    <?php
                        foreach ($khoa as $key => $value) {
                            echo "\t<option value=\"{$key}\">{$value}</option>\n";
                        };
                    ?>
                </select>
            </div>
        </div>

        <div class="submit">
            <button>Đăng ký</button>
        </div>

    </div>

</form>